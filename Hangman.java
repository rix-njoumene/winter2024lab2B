import java.util.Scanner;


public class Hangman{

	/*
	*verify if the guessed char is in the word and return its position
	*if guessed char is not in word, return -1
	*/
	public static int isLetterInWord(String word, char c){
		for(int i = 0; i < word.length(); i++){
			if( toUpperCase( word.charAt(i) ) == toUpperCase(c) ){
				return i;
			}
		}
		return -1;
	}
	
	/*
	*lower case char transformed to upper case
	*/
	public static char toUpperCase(char c){
		return Character.toUpperCase(c);
	}
	
	
	/*
	*Summary of the letter that have been found by the player.
	*if a letter is found, it would be printed in the summary message of the letter found.
	*if not, a '-' would be placed instead to indicate that the letter wasn't found.
	*/
	public static void printWord(String word, boolean[] isRightLetter){
		String guessedWord = "";

		for(int i = 0; i < isRightLetter.length; i++){
			if(isRightLetter[i]){
				guessedWord += word.charAt(i);
			}
			
			else{
				guessedWord += "-";
			}
		}
			
		System.out.println(guessedWord);
	}
	
	/*
	*Initiate the Game loop of the game
	*the game end if the victory state is reached or 6 mistakes are made.
	*It pronpt the user to guess a letter, then verify if it exist in the mystery word
	*If not found, add +1 to numMisses and print a Mistake message
	*if found, add +1 to numRightLetters and print a Found message
	*print a summary of the letters found after each round
	*/
	
	public static void runGame(String word){
		
		Scanner reader = new Scanner(System.in);
		
		boolean[] isRightLetter = new boolean[4];
		boolean victory = false;
		int numRightLetters = 0;
		int numMisses = 0;
		
		
		while(!victory && numMisses < 6){
			
			System.out.println("Try to guess a letter! Enter your choice in the command line.");
			char guess = reader.nextLine().charAt(0);

			int letterPos = isLetterInWord(word, guess);
			
			if (letterPos < 0){
				numMisses++;
				System.out.println("You made a mistake!Now you only have " + (6 - numMisses) + " chances left, be careful!");
			}
			else{
				System.out.println("Your guess was right! Continue like that and you might win this game!");
				
				for(int i = 0; i< isRightLetter.length; i++){
					if(letterPos == i){
						if (!isRightLetter[i]){
							isRightLetter[i] = true;
							numRightLetters += 1;
						}	
					}
				}
			}
			
		
			if(numRightLetters == 4){
				victory = true;
				}
			
			System.out.println("Letter(s) found: " );
			printWord(word, isRightLetter);
				
		}
		
		if(victory){
			System.out.println("Bravo, You WON the game!");
		}
		else{
			System.out.println("Too bad, you LOST the game!");
		}
	}
}