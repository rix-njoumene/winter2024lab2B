import java.util.Random;
import java.util.Scanner;


public class Wordle{
	
	/*
	*Choose a random mystery word among the wordsBank array and return it
	*/
	
	public static String generateWord(){
		
		String[] wordsBank = {
			"light", "might", "white", "flake", "poise", 
			"noise", "voice", "vocal", "abode", "brave", 
			"crave", "dream", "steam", "stare", "flare",
			"great", "adept", "ageism", "shock", "anime"
		};
		
		Random rand = new Random();
		
		String word = wordsBank[rand.nextInt(wordsBank.length-1)];
		
		return word;
	}
	
	
	public static boolean letterInWord(String word, char letter){
		
		for(int i = 0; i < word.length(); i++){
			
			if( Character.toUpperCase(word.charAt(i)) == letter ){
				return true;
			}
		}
		
		return false;
		
	}
	
	public static boolean letterInSlot(String word, char letter, int position){
		
		if(Character.toUpperCase(word.charAt(position)) == letter){
			return true;
		}
		else{
			return false;
		}
	
	}
	
	public static String[] guessWord(String answer, String guess){
		Scanner reader = new Scanner(System.in);
		String[] verify = new String[1];
		
		//Ask the user to input a guess
		if(answer.equals("input") && guess.equals("guess") ){
			
			System.out.println("Try to guess the word!" + System.lineSeparator() + "Enter a 5 letters word to try to guess the answer!");
			verify[0] = reader.nextLine();
			return verify;
		}
		
		// Ask again the user to enter a valid guess
		if(answer.equals("input") && guess.equals("wrongLength") ){
			
			System.out.println("");
			
			System.out.println("Your word wasn't of length 5, thus it couldn't be processed" + System.lineSeparator() 
			+ "This time, try to enter a valid 5 letters word to try to guess the answer! ");
			verify[0] = reader.nextLine();
			return verify;
		}
		
		// Colors of each letter
		else{
	
				String[] colorsPosition = new String[answer.length()];
			
			char[] guessLetters = new char[guess.length()];
			
			for(int i = 0; i < guess.length(); i++){
				guessLetters[i] = guess.charAt(i);
				
			}
			
			
			for(int i = 0; i < answer.length(); i++){
				
				int letterPosition = i;
				
				if(letterInWord(answer, guessLetters[i]) == true){
								
					if(letterInSlot(answer, guessLetters[i], letterPosition) == true){
								
						colorsPosition[letterPosition] = "green";
					}
					else{
						
						colorsPosition[letterPosition] = "yellow";
					}
				}
				else{
					
					colorsPosition[letterPosition] = "white";
				}
			}
			
			return colorsPosition;
		}
	}

	public static int presentResults(String word, String[] colours){
		
		// Colors formating
		String[] colorsFormatting = new String[word.length()];
		
		final String Reset = "\u001B[0m"; 
		final String Yellow = "\u001B[33m";
		final String Green = "\u001B[32m";
		
		for(int i = 0; i < word.length(); i++){
			
					
			
			if(colours[i].equals("green") ){
						
				colorsFormatting[i] = "1" + Green + word.charAt(i) + Reset;
			}
			if(colours[i].equals("yellow") ){
				
				colorsFormatting[i] = "2" + Yellow + word.charAt(i) + Reset;
			}
			if(colours[i].equals("white") ){
				
				colorsFormatting[i] = "0" + word.charAt(i);
			}
						
		}
		
		// Winning Conditions
		int numMisses = 0;
		int wrongSlot = 0;
		int goodSlot = 0;
		
		// Counter of Green, Yellow and White letters
		for(int i = 0; i < word.length(); i++){
			
			if( colorsFormatting[i].substring(0, 1).equals("1") ){
				
				colorsFormatting[i] = colorsFormatting[i].substring(1);
				goodSlot++;
			}
			if( colorsFormatting[i].substring(0, 1).equals("2") ){
				
				colorsFormatting[i] = colorsFormatting[i].substring(1);
				wrongSlot++;
			}
			if( colorsFormatting[i].substring(0, 1).equals("0") ){
					
				colorsFormatting[i] = colorsFormatting[i].substring(1);
				numMisses++;
			}
			
		}
		
		// Printing the Colored User Guess
		String coloredGuess = "";
		
		for(int i = 0; i < word.length(); i++){
			
			coloredGuess += colorsFormatting[i];
		}
		
		System.out.println("Your Answer: " + System.lineSeparator() + coloredGuess);
		System.out.println("");
		
		// Winning State
		if(goodSlot == word.length() ){
			System.out.println("Bravo, You WON the game!");
			return -1;
		}
		
		// Mistakes + Try Again State
		else{
			System.out.println("Your guess was wrong: " + wrongSlot + " Misplaced letters and " + numMisses + " Non-existing letters, try again!");
			return 1;
		}
	}
	
	//verify the length of the guess according to the word
	public static String readGuess(){
		
		String verify = guessWord("input", "guess")[0];
		
		while(verify.length() != 5){
			
			verify = guessWord("input", "wrongLength")[0];
		}
		
		return verify.toUpperCase();
		
	}
	
	public static void runGame(String word){
		
		
		// Winning State and Number of Tries left
		int numsTry = 0;
		
		while(numsTry > -1 && numsTry != 6 ){
			
			String wordToGuess = readGuess();
			
			if( presentResults(wordToGuess, guessWord(word, wordToGuess )) == -1){
				numsTry = -1;
			}
			else{
				numsTry++;
				
				//Losing game State and tries Left
				if(numsTry <= 6 ){
					
					if(numsTry < 6){
					System.out.println("You have " + (6-numsTry) + " tries left! Be careful!");
					}
					
					if(numsTry == 6){
						System.out.println("Too bad, you LOST the game!");
					}
				}
			} 
			
		}
		
		
	}
	
}