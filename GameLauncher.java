import java.util.Scanner;


public class GameLauncher{
	
	/*
	*Welcome the User and ask which game to run
	*[1] would run Hangman
	*[2] would run Wordle
	*/
	
	public static void main (String args[]){
		
		System.out.println("\nWelcome New User on... \nRix's Amazing Game Launcher!");
		
		System.out.println("\nThere is currently two games avaible on our platform. \nWhich one would you like to play?");
		System.out.println("\n[1]Hangman \n[2]Wordle");
		
		Scanner reader = new Scanner(System.in);
		System.out.print("Enter your choice: ");
		int gameChoice = reader.nextInt();
		
		//HANGMAN Game
		if(gameChoice == 1){
			
			System.out.println("\nYou just entered the HANGMAN Game \n... \nHave an Amazing Game Session!");
			
			Scanner readerHangman = new Scanner(System.in);
			
			System.out.println("\nEnter a 4 letter word that would have to be guessed! ");
			String word = readerHangman.nextLine();
			
			Hangman.runGame(word);
			
		}
		
		//WORDLE Game
		if(gameChoice == 2){
			
			System.out.println("\nYou just entered the WORDLE Game \n... \nHave an Amazing Game Session!");
			
			Wordle.runGame( Wordle.generateWord() );
			
		}
		
	}
	
	
}